import os
from glob import glob
from setuptools import find_packages, setup

package_name = 'planar_robot_python'

setup(
    name=package_name,
    version='0.0.0',
    packages=find_packages(exclude=['test']),
    data_files=[
        ('share/ament_index/resource_index/packages',
            ['resource/' + package_name]),
        ('share/' + package_name, ['package.xml']),
        (os.path.join('share', package_name, 'launch'), glob(os.path.join('launch', '*launch.[pxy][yma]*'))),
        (os.path.join('share', package_name, 'urdf'), glob(os.path.join('urdf/*')))
    ],
    install_requires=['setuptools'],
    zip_safe=True,
    maintainer='joao',
    maintainer_email='joao.cavalcanti-santos@lirmm.fr',
    description='TODO: Package description',
    license='TODO: License declaration',
    tests_require=['pytest'],
    entry_points={
        'console_scripts': [
            'trajectory_generator   = planar_robot_python.trajectory_generator:main',
            'trajectory_sample      = planar_robot_python.trajectory_sample:main',
            'high_level_manager     = planar_robot_python.high_level_manager:main',
            'controller             = planar_robot_python.controller:main',
            'simulator              = planar_robot_python.simulator:main',
            'kinematic_model        = planar_robot_python.kinematic_model:main',
            'disturbance            = planar_robot_python.disturbance:main',
        ],
    },
)
